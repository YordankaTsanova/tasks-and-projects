package com.telerikacademy.cosmetics.models.common;

public enum GenderType {
    MEN("Men"),
    WOMEN("Women"),
    UNISEX("Unisex");


    private String name;

    private GenderType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
