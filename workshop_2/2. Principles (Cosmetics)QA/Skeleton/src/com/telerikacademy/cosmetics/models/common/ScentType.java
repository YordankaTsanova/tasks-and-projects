package com.telerikacademy.cosmetics.models.common;

public enum ScentType {
    LAVENDER("Lavender"),
    VANILLA("Vanilla"),
    ROSE("Rose");


    private String name;

    private ScentType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    

}
