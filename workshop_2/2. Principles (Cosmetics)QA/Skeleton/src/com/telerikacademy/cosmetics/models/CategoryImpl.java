package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;


import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.cosmetics.models.products.ValidationClass.validationCategoryNameLength;
import static com.telerikacademy.cosmetics.models.products.ValidationClass.validationProductIsNotNull;

public class CategoryImpl implements Category {
    //use constants for validations values
    
    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public void setName(String name) {
        validationCategoryNameLength(name);
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //We return a copy to protect the real one from external changes.
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        validationProductIsNotNull(product);
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("Product not found in category.");
        }
        products.remove(product);
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s%n" +
                    " #No product in this category", name);
        }
        StringBuilder result = new StringBuilder();
        result.append(String.format("#Category: %s%n", name));

        if (products.isEmpty()) {
            result.append(" #No product in this category%n");
        }

        for (Product product : products) {
            result.append(product.print());
        }

        return result.toString();
    }
    
}
