package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

import static com.telerikacademy.cosmetics.models.products.ValidationClass.validationMilliliters;


public class ShampooImpl extends ProductBase implements Shampoo {

    private int milliliters;
    private UsageType everyDay;
    
    public ShampooImpl(String name, String brand, double price, GenderType gender
            , int milliliters, UsageType everyDay) {

        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setEveryDay(everyDay);
    }

    private void setMilliliters(int milliliters) {
        validationMilliliters(milliliters);
        this.milliliters = milliliters;
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    private void setEveryDay(UsageType everyDay) {
        this.everyDay = everyDay;
    }
    @Override
    public UsageType getUsage() {
        return UsageType.valueOf(String.valueOf(everyDay));
    }


    @Override
    public String print() {
        return String.format("#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s%n" +
                " #Milliliters: %d%n" +
                " #Usage: %s%n" +
                " ===", super.getName(), super.getBrand(), super.getPrice(), super.getGender().getName(), milliliters, everyDay);
    }
}
