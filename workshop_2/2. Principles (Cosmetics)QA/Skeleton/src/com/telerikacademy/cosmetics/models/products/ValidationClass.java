package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.contracts.Product;

import java.util.List;

public class ValidationClass {

    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_NAME_LENGTH = 2;
    private static final int MAX_BRAND_NAME_LENGTH = 10;
    private static final int MIN_CATEGORY_NAME_LENGTH = 2;
    private static final int MAX_CATEGORY_NAME_LENGTH = 15;


    public static void nullOrEmptyName(String name) {
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException("Name cannot be null or empty.");
        }
    }

    public static void validationNameLength(String name) {
        if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException
                    ("Minimum shampoo name’s length is 3 symbols and maximum is 10 symbols.");
        }
    }

    public static void validationBrandNameLength(String brand) {
        if (brand.length() < MIN_BRAND_NAME_LENGTH || brand.length() > MAX_BRAND_NAME_LENGTH) {
            throw new IllegalArgumentException
                    ("Minimum shampoo brand name’s length is 2 symbols and maximum is 10 symbols.");
        }
    }

    public static void validationPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
    }

    public static void validationMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters are not negative number");
        }
    }

    public static void validationCategoryNameLength(String name) {
        if (name.length() < MIN_CATEGORY_NAME_LENGTH || name.length() > MAX_CATEGORY_NAME_LENGTH) {
            throw new IllegalArgumentException
                    ("Name should be between 2 and 15 symbols.");
        }
    }

    public static void validationProductIsNotNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("Product can't be null.");
        }
    }

    public static void validationIngredientsIsNotNull(List<String> ingredients) {
        if (String.valueOf(ingredients) == null) {
            throw new IllegalArgumentException("Ingredients cannot be null.");
        }
    }
}
