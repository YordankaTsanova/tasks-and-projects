package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;


import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.cosmetics.models.products.ValidationClass.validationIngredientsIsNotNull;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(new ArrayList<>(ingredients));
    }

    private void setIngredients(List<String> ingredients) {
        validationIngredientsIsNotNull(ingredients);
        this.ingredients = new ArrayList<>(ingredients);
    }

    @Override
    public List<String> getIngredients() {
        return  new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s%n" +
                " #Ingredients: %s%n" +
                " ===", super.getName(),  super.getBrand(), super.getPrice(), super.getGender().getName(), ingredients);

    }
}
