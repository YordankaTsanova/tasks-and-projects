package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;

import static com.telerikacademy.cosmetics.models.products.ValidationClass.*;


public class ProductBase implements Product {

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    //Finish the class
    //What variables, what constants should you write here?
    //validate
    
    ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    private void setName(String name) {
        nullOrEmptyName(name);
        validationNameLength(name);
        this.name = name;
    }
    @Override
    public String getName() {
        return name;
    }

    private void setBrand(String brand) {
        nullOrEmptyName(brand);
        validationBrandNameLength(brand);
        this.brand = brand;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    private void setPrice(double price) {
        validationPrice(price);
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    @Override
    public GenderType getGender() {
       return GenderType.valueOf(String.valueOf(gender));
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                " #Price: $%.2f%n" +
                " #Gender: %s%n" +
                " ===", this.name, this.brand, this.price, this.gender.name());
    }
}
