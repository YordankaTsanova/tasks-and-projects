package com.telerikacademy.dealership;

import com.telerikacademy.dealership.core.DealershipEngineImpl;

public class Startup {
    
    public static void main(String[] args) {
        DealershipEngineImpl engine = new DealershipEngineImpl();
        engine.start();
    }
    
}
