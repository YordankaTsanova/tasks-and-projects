package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.ArrayList;
import java.util.List;

public class CarImpl extends VehicleBase implements Car {

    private final static VehicleType vehicleType = VehicleType.CAR;

    private final static int MIN_SEATS = 1;
    private final static int MAX_SEATS = 10;

    private final static String SEATS_EXCEPTION_MESSAGE = String.format("Seats must be between %d and %d!"
            , MIN_SEATS, MAX_SEATS);
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, vehicleType);
        setSeats(seats);
    }

    private void setSeats(int seats) {
        Validator.ValidateIntRange(seats, MIN_SEATS, MAX_SEATS, SEATS_EXCEPTION_MESSAGE);
        this.seats = seats;
    }

    @Override
    public int getSeats() {
        Validator.ValidateIntRange(seats, MIN_SEATS, MAX_SEATS, SEATS_EXCEPTION_MESSAGE);
        return seats;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(super.getComments());
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Seats: %d", getSeats());
    }

}
