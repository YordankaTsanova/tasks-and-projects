package com.telerikacademy.dealership.models.contracts;

public interface Car extends Vehicle {
    
    int getSeats();
    
}
