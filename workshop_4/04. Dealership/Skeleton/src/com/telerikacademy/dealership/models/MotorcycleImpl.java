package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import java.util.ArrayList;
import java.util.List;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private final static VehicleType vehicleType = VehicleType.MOTORCYCLE;

    private final static int MIN_CATEGORY_LENGTH = 3;
    private final static int MAX_CATEGORY_LENGTH = 10;

    private final static String CATEGORY_EXCEPTION_MESSAGE = String.format("Category must be between %d and %d characters long!"
            , MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH);
    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, vehicleType);
        setCategory(category);
    }

    private void setCategory(String category) {
        Validator.ValidateNull(category, CATEGORY_EXCEPTION_MESSAGE);
        Validator.ValidateIsNotEmpty(category, CATEGORY_EXCEPTION_MESSAGE);
        Validator.ValidateIntRange(category.length(), MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH, CATEGORY_EXCEPTION_MESSAGE);
        this.category = category;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(super.getComments());
    }

    @Override
    protected String printAdditionalInfo() {

        return String.format("  Category: %s", getCategory());
    }
}
