package com.telerikacademy.dealership.models.contracts;

public interface Priceable {
    
    double getPrice();
    
}
