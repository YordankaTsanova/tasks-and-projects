package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    private final static double MIN_PRICE = 0.0;
    private final static double MAX_PRICE = 1000000.0;

    private final static String PRICE_EXCEPTION_MESSAGE = String.format("Price must be between %.1f and %.1f!"
            , MIN_PRICE, MAX_PRICE);

    private final static int MIN_MAKE_LENGTH = 2;
    private final static int MAX_MAKE_LENGTH = 15;

    private final static String MAKE_EXCEPTION_MESSAGE = String.format("Make must be between %d and %d characters long!"
            , MIN_MAKE_LENGTH, MAX_MAKE_LENGTH);

    private final static int MIN_MODEL_LENGTH = 1;
    private final static int MAX_MODEL_LENGTH = 15;

    private final static String MODEL_EXCEPTION_MESSAGE = String.format("Make must be between %d and %d characters long!"
            , MIN_MODEL_LENGTH, MAX_MODEL_LENGTH);
    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;
    private List<Comment> comments;


    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);

        comments = new ArrayList<>();
    }

    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public int getWheels() {
        return this.vehicleType.getWheelsFromType();
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    private void setMake(String make) {
        Validator.ValidateNull(make, MAKE_EXCEPTION_MESSAGE);
        Validator.ValidateIsNotEmpty(make, MAKE_EXCEPTION_MESSAGE);
        Validator.ValidateIntRange(make.length(), MIN_MAKE_LENGTH, MAX_MAKE_LENGTH, MAKE_EXCEPTION_MESSAGE);
        this.make = make;
    }

    private void setModel(String model) {
        Validator.ValidateNull(model, MODEL_EXCEPTION_MESSAGE);
        Validator.ValidateIsNotEmpty(model, MODEL_EXCEPTION_MESSAGE);
        Validator.ValidateIntRange(model.length(), MIN_MODEL_LENGTH, MAX_MAKE_LENGTH, MODEL_EXCEPTION_MESSAGE);
        this.model = model;
    }

    private void setPrice(double price) {
        Validator.ValidateDecimalRange(price, MIN_PRICE, MAX_PRICE, PRICE_EXCEPTION_MESSAGE);
        this.price = price;
    }

    private void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  " + MAKE_FIELD + ": %s", this.getMake())).append(System.lineSeparator());
        builder.append(String.format("  " + MODEL_FIELD + ": %s", this.getModel())).append(System.lineSeparator());
        builder.append(String.format("  " + WHEELS_FIELD + ": %d", this.getWheels())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //todo replace this comment with explanation why this method is protected:
    // Protected access gives the subclass a chance to use the helper method or variable
    // , while preventing a non-related class from trying to use it.
    protected abstract String printAdditionalInfo();

    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

}
