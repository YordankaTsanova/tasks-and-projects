## How to run JMeter file:

1. Open the JMeter from .bat file in bin folder
2. Click on the "File" button and select "Open"
3. In the open window find the file with extension .jmx, click on it and click "Open" button
4. There are two options for starting the tests in the Test plan:
- From the start button, which is located at the top of the button bar and it is green 
- If you go on specific Thread Group, click on it with right button of the mouse and select "Start" from the 
drop-down menu