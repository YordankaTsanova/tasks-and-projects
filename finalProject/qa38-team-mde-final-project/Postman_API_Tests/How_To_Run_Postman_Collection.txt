
/*------------Run test cases from Command Prompt----------------------------------------------------*/

3. To run a Postman collection from Command Prompt, follow these steps:


Step 1: Open the Command Prompt

Step 2: To go in the directory where is the .bat file, which you want to run, in the Command Prompt execute the command:
cd <path to the file on your computer>

Step 3: To run the .bat file in the Command Prompt write: <name of the file>


The name of the .bat file under this project is: Postman_API_Tests.bat
