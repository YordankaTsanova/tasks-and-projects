package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;

public class LatestPostsPage extends BaseWebsitePage {
    public LatestPostsPage(WebDriver driver) {
        super(driver, "latest.posts.page");
    }

    public void clickBrowsePublicPostsButton() {

        actions.waitForElementVisible("explore.posts.page.browse.public.button");
        actions.clickElement("explore.posts.page.browse.public.button");
    }

    public void selectPostByPostId(int postId) {

        actions.waitForElementVisible(format((getUIMappingByKey("explore.posts.page.explore.post.button")), postId));
        actions.clickElement(format((getUIMappingByKey("explore.posts.page.explore.post.button")), postId));

    }

}
