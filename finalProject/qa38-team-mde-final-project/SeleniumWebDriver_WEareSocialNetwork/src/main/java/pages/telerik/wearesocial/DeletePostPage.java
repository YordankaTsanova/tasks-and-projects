package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static java.lang.String.format;

public class DeletePostPage extends BaseWebsitePage {
    public DeletePostPage(WebDriver driver, int postId) {
        super(driver, format((getConfigPropertyByKey("personal.delete.post.page")), postId));
    }

    public void deletePost(boolean confirmDeletePost) {

        String locator;
        if (confirmDeletePost) {
            locator = "delete.post.page.confirm.delete";
        } else {
            locator = "delete.post.page.confirm.cancel";
        }

        actions.waitForElementClickable("delete.post.page.confirm");
        actions.clickElement("delete.post.page.confirm");
        actions.clickElement(locator);

        actions.waitForElementClickable("delete.post.page.submit.button");
        actions.clickElement("delete.post.page.submit.button");

    }

}
