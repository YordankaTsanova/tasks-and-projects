package pages.telerik.wearesocial;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class BaseWebsitePage extends BasePage {
    public BaseWebsitePage(WebDriver driver, String urlKey) {
        super(driver, urlKey);
    }
}