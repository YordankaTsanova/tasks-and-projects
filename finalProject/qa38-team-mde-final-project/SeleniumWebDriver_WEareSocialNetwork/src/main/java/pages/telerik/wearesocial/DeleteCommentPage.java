package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static java.lang.String.format;

public class DeleteCommentPage extends BaseWebsitePage {

    public DeleteCommentPage(WebDriver driver, int commentId) {
        super(driver, format((getConfigPropertyByKey("personal.delete.comment.page")), commentId));
    }

    public void deleteComment(boolean confirmDeleteComment) {

        String locator;
        if (confirmDeleteComment) {
            locator = "delete.post.page.confirm.delete";
        } else {
            locator = "delete.post.page.confirm.cancel";
        }

        actions.waitForElementClickable("delete.post.page.confirm");
        actions.clickElement("delete.post.page.confirm");
        actions.clickElement(locator);

        actions.waitForElementClickable("delete.post.page.submit.button");
        actions.clickElement("delete.post.page.submit.button");

    }

}
