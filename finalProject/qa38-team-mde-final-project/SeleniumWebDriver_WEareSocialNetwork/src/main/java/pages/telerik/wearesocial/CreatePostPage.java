package pages.telerik.wearesocial;

import org.openqa.selenium.WebDriver;

public class CreatePostPage extends BaseWebsitePage {

    public CreatePostPage(WebDriver driver) {
        super(driver, "create.post.page");
    }

    public void createPost(String postContent, boolean visibility) {

        String locator;
        if (visibility) {
            locator = "create.post.page.visibility.public";
        } else {
            locator = "create.post.page.visibility.private";
        }

        navigateToPage();

        actions.waitForElementClickable("create.post.page.visibility");
        actions.clickElement("create.post.page.visibility");
        actions.clickElement(locator);

        actions.clickElement("create.post.page.post.message");
        actions.typeValueInField(postContent, "create.post.page.post.message");

        actions.waitForElementClickable("create.post.page.save.post.button");
        actions.clickElement("create.post.page.save.post.button");

    }

}

