package pages.telerik.wearesocial;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;

public class ExplorePostPage extends BaseWebsitePage {

    public ExplorePostPage(WebDriver driver, int postId) {
        super(driver, format((getConfigPropertyByKey("personal.explore.post.page")), postId));
    }

    public void clickEditPost(int postId) {

        actions.waitForElementVisible(format((getUIMappingByKey("explore.post.page.edit.button")), postId));
        actions.clickElement(format((getUIMappingByKey("explore.post.page.edit.button")), postId));
    }

    public void clickDeletePost(int postId) {
        actions.waitForElementVisible(format((getUIMappingByKey("explore.post.page.delete.button")), postId));
        actions.clickElement(format((getUIMappingByKey("explore.post.page.delete.button")), postId));

    }

    public int[] addComment(String comment) {

        int[] arrayIncreaseAndIdReturn = new int[2];

        WebElement e = actions.getDriver().findElement(By.xpath("//h3[@id='com']"));
        JavascriptExecutor js = (JavascriptExecutor) actions.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", e);
        String numberComments = e.getText();
        String[] arr = numberComments.split(" ");
        int initialNumber = Integer.parseInt(arr[0]);

        actions.waitForElementVisible("explore.post.page.current.comment.message");
        actions.typeValueInField(comment, ("explore.post.page.current.comment.message"));

        actions.waitForElementClickable("explore.post.page.post.comment.button");
        actions.clickElement("explore.post.page.post.comment.button");

        e = actions.getDriver().findElement(By.xpath("//h3[@id='com']"));
        js.executeScript("arguments[0].scrollIntoView();", e);
        numberComments = e.getText();
        arr = numberComments.split(" ");
        int finalNumber = Integer.parseInt(arr[0]);

        List<WebElement> listOfComments = actions.getDriver().findElements(By.xpath(getUIMappingByKey("explore.post.page.comments.list")));
        int commentId = Integer.parseInt(listOfComments.get(listOfComments.size() - 1).getAttribute("id"));

        arrayIncreaseAndIdReturn[0] = finalNumber - initialNumber;
        arrayIncreaseAndIdReturn[1] = commentId;
        return arrayIncreaseAndIdReturn;
    }

    public int editComment(int commentId, String comment) {

        WebElement e = actions.getDriver().findElement(By.xpath("//h3[@id='com']"));
        JavascriptExecutor js = (JavascriptExecutor) actions.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", e);
        String numberComments = e.getText();
        String[] arr = numberComments.split(" ");
        int initialNumber = Integer.parseInt(arr[0]);

        actions.waitForElementClickable("explore.post.page.show.comments.button");
        actions.clickElement("explore.post.page.show.comments.button");

        actions.waitForElementVisible(format((getUIMappingByKey("explore.post.page.comment.toEdit")), commentId));
        actions.clickElement(format((getUIMappingByKey("explore.post.page.comment.toEdit")), commentId));

        actions.waitForElementVisible("explore.post.page.current.comment.message");
        actions.typeValueInField(comment, ("explore.post.page.current.comment.message"));

        actions.waitForElementClickable("explore.post.page.comment.edit.button");
        actions.clickElement("explore.post.page.comment.edit.button");

        e = actions.getDriver().findElement(By.xpath("//h3[@id='com']"));
        js.executeScript("arguments[0].scrollIntoView();", e);
        numberComments = e.getText();
        arr = numberComments.split(" ");
        int finalNumber = Integer.parseInt(arr[0]);

        return (finalNumber - initialNumber);

    }

    public void deleteComment(int commentId) {

        WebElement e = actions.getDriver().findElement(By.xpath("//h3[@id='com']"));
        JavascriptExecutor js = (JavascriptExecutor) actions.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", e);
        String numberComments = e.getText();
        String[] arr = numberComments.split(" ");

        actions.waitForElementClickable("explore.post.page.show.comments.button");
        actions.clickElement("explore.post.page.show.comments.button");

        actions.waitForElementVisible(format((getUIMappingByKey("explore.post.page.delete.comment.button")), commentId));
        actions.clickElement(format((getUIMappingByKey("explore.post.page.delete.comment.button")), commentId));

    }

}