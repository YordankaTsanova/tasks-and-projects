package test.cases;

import com.telerikacademy.testframework.data.Constants;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.telerik.wearesocial.*;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;

public class EditPostTest extends BaseTestSetup {

       @Test
    public void successfulEditPostTest() {

        login();

        PersonalHomePage personalHomePage = new PersonalHomePage(actions.getDriver());
        personalHomePage.navigateToPersonalProfilePage();

        PersonalProfilePage personalProfilePage = new PersonalProfilePage(actions.getDriver(),41);
        personalProfilePage.selectPostByPostId(Constants.postId);

        ExplorePostPage explorePostPage = new ExplorePostPage(actions.getDriver(), Constants.postId);
        explorePostPage.clickEditPost(Constants.postId);

        EditPostPage editPostPage = new EditPostPage(actions.getDriver(),Constants.postId);
        editPostPage.editPost(postEditMessage,postEditVisibility);

        String getUserName =actions.getDriver().findElement(By.xpath(format(getUIMappingByKey
                                                 ("explore.post.page.user.name")))).getText();
        String getPostMessage =actions.getDriver().findElement(By.xpath(format(getUIMappingByKey
                                                 ("explore.post.page.current.post")))).getText();

        Assert.assertEquals("Post username is not correct",username,getUserName);
        Assert.assertEquals("Post message is not correct",postEditMessage,getPostMessage);

        personalProfilePage.logout();

    }

}

