package test.cases;

import com.telerikacademy.testframework.data.Constants;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.telerik.wearesocial.CreatePostPage;
import pages.telerik.wearesocial.PersonalHomePage;

import java.util.List;

import static com.telerikacademy.testframework.Utils.getUIMappingByKey;

public class CreatePostTest extends BaseTestSetup {

    @Test
    public void successfulCreatePostTest() {

        login();

        PersonalHomePage personalHomePage = new PersonalHomePage(actions.getDriver());
        personalHomePage.navigateToCreatePostPage();

        CreatePostPage createPostPage = new CreatePostPage(actions.getDriver());
        createPostPage.createPost(postCreateMessage, postCreateVisibility);

        List<WebElement> listOfPosts = actions.getDriver().findElements(By.xpath(getUIMappingByKey
                                                                    ("explore.posts.page.posts")));
        String getUserName =listOfPosts.get(0).findElement(By.tagName("h2")).getText();
        String getPostMessage =listOfPosts.get(0).findElement(By.xpath(getUIMappingByKey
                                             ("explore.post.page.current.post.message"))).getText();

        Assert.assertEquals("Post username is not correct",username,getUserName);
        Assert.assertEquals("Post message is not correct",postCreateMessage,getPostMessage);

        Constants.postId= Integer.parseInt(listOfPosts.get(0).findElement(By.xpath(getUIMappingByKey
                ("explore.personal.posts.page.postId"))).getAttribute("value"));

    }

}