package test.cases;

import com.telerikacademy.testframework.data.Constants;
import org.junit.Test;
import pages.telerik.wearesocial.*;

public class DeleteCommentTest extends BaseTestSetup{
    String tempUsername;

    @Test
    public void successfulDeleteCommentToPostTest() {
        tempUsername=username;
        username=usernameComment;
        login();
        username=tempUsername;

        PersonalHomePage personalHomePage = new PersonalHomePage(actions.getDriver());
        personalHomePage.navigateToLatestPostsPage();

        LatestPostsPage latestPostsPage= new LatestPostsPage(actions.getDriver());
        latestPostsPage.clickBrowsePublicPostsButton();
        latestPostsPage.selectPostByPostId(Constants.postId);

        ExplorePostPage explorePostPage = new ExplorePostPage(actions.getDriver(), Constants.commentId);
        explorePostPage.deleteComment(Constants.commentId);

        DeleteCommentPage deleteCommentPage = new DeleteCommentPage(actions.getDriver(),Constants.commentId);
        deleteCommentPage.deleteComment(true);

        actions.assertElementPresent("delete.post.page.delete.comment.confirmed");

    }

}





