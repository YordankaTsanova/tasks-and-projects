package test.cases;

import org.junit.Test;
import pages.telerik.wearesocial.LoginPage;

import static com.telerikacademy.testframework.Utils.LOGGER;

public class LoginTest extends BaseTestSetup {

    @Test
    public void successfulUserLoginTest() {
        registration();
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser(username, PASSWORD);

        actions.assertElementPresent("personal.profile.button");
        LOGGER.info("User is logged successfully");
    }

}