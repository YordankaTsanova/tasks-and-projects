## Tasks And Projects

### This repository contains the main tasks and projects from my Alpha QA training at Telerik Academy.

1. The finalProject folder contains tests conducted on a test object that was created by colleagues trained in the Java program at Telerik Academy. 
   Exploratory testing using Xray Exploratory App; Rest API testing with Postman and UI tests with Selenium WebDriver were prepared by the MDE team: 
   Magdalena Takeva, Yordanka Tsanova and Eli Nedyalkova. In addition, were prepared by me Performance testing with JMeter and CI/CD Pipelines in GitLab of the Postman collection. 
   More detailed description of the project can be found in the README.md file that is located in the finalProject folder.

2. The postman folder contains a collection with tests for Trello, which is a web-based, Kanban-style, list-making application.

3. Folders workshop_2 and workshop_3 are with home works related to Java and OOP principles. 
   More detailed descriptions of the individual tasks can be found in the README.md files, which are located in the respective folders: workshop_2 and workshop_3/Skeleton.

4. The workshop_4 folder contains task from exam related to Java and OOP principles.
   More detailed description of the task can be found in the README.md file that is located in workshop_4/Skeleton.

