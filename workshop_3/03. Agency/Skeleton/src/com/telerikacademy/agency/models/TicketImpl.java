package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class TicketImpl implements Ticket, Journey{

    private Journey journey;
    private double administrativeCosts;

    private String destination;
    private double price;
    
    public TicketImpl(Journey journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    private void setJourney(Journey journey) {
        this.journey = journey;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double calculatePrice() {
        return price;
    }

    @Override
    public String getDestination() {
        return destination;
    }


    @Override
    public int getDistance() {
        return 0;
    }

    @Override
    public String getStartLocation() {
        return null;
    }

    @Override
    public Vehicle getVehicle() {
        return null;
    }

    @Override
    public double calculateTravelCosts() {
        return 0;
    }

    @Override
    public String print() {
        return String.format("Ticket ----" + System.lineSeparator() +
                "Destination: %s" + System.lineSeparator() +
                "Price: %.2f" + System.lineSeparator()
                , getDestination(), price);
    }

    @Override
    public String toString() {
        return print();
    }

}
