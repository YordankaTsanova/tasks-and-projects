package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl implements Bus {

    private static final int MIN_PASSENGER_CAPACITY = 10;
    private static final int MAX_PASSENGER_CAPACITY = 50;

    private static final String MESSAGE_PASSENGER_CAPACITY = String.format
            ("A bus cannot have less than %d passengers or more than %d passengers."
            , MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY);
    private int passengerCapacity;
    private double pricePerKilometer;


    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public int getPassengerCapacity() {
        return this.passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw  new IllegalArgumentException(MESSAGE_PASSENGER_CAPACITY);
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return this.pricePerKilometer;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }

    @Override
    public String print() {
        return String.format("Bus ----" + System.lineSeparator() +
                "Passenger capacity: %d" + System.lineSeparator() +
                "Price per kilometer: %.2f" + System.lineSeparator() +
                "Vehicle type: %s" + System.lineSeparator()
                , this.passengerCapacity, this.pricePerKilometer, getType());
    }

    @Override
    public String toString() {
        return print();
    }

}
