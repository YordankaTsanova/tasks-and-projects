package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {

    private final String TYPE_CANT_BE_NULL = "The type cannot be null";

    private static final int MIN_PASSENGER_CAPACITY = 1;
    private static final int MAX_PASSENGER_CAPACITY = 800;

    private static final double MIN_PRICE_PER_KILOMETER = 0.10;
    private static final double MAX_PRICE_PER_KILOMETER = 2.50;
    private static final String MESSAGE_PASSENGER_CAPACITY = String.format
            ("A vehicle with less than %d passengers or more than %d passengers cannot exist!"
                    , MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY);

    private static final String MESSAGE_PRICE_PER_KILOMETER = String.format
            ("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!"
                    , MIN_PRICE_PER_KILOMETER, MAX_PRICE_PER_KILOMETER);
   private int passengerCapacity;
   private double pricePerKilometer;
   private VehicleType type;

    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(MESSAGE_PASSENGER_CAPACITY);
        }
        this.passengerCapacity = passengerCapacity;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE_PER_KILOMETER || pricePerKilometer > MAX_PRICE_PER_KILOMETER) {
            throw new IllegalArgumentException(MESSAGE_PRICE_PER_KILOMETER);
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String print() {
        return String.format("Train ----" + System.lineSeparator() +
                "Passenger capacity: %d" + System.lineSeparator() +
                "Price per kilometer: %.2f" + System.lineSeparator() +
                "Vehicle type: %s" + System.lineSeparator() +
                "####################" , this.passengerCapacity, this.pricePerKilometer, getType());
    }

    @Override
    public String toString() {
        return print();
    }
}
