package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl implements Train {

    private static final int MIN_PASSENGER_CAPACITY = 30;
    private static final int MAX_PASSENGER_CAPACITY = 150;

    private static final int MIN_QUANTITY_CARTS = 1;
    private static final int MAX_QUANTITY_CARTS = 15;

    private static final String MESSAGE_PASSENGER_CAPACITY = String.format
            ("A train cannot have less than %d passengers or more than %d passengers."
                    , MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY);

    private static final String MESSAGE_CARTS = String.format
            ("A train cannot have less than %d cart or more than %d carts."
                    , MIN_QUANTITY_CARTS, MAX_QUANTITY_CARTS);
    private int passengerCapacity;
    private double pricePerKilometer;
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setCarts(carts);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw  new IllegalArgumentException(MESSAGE_PASSENGER_CAPACITY);
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    private void setCarts(int carts) {
        if (carts < MIN_QUANTITY_CARTS || carts > MAX_QUANTITY_CARTS) {
            throw new IllegalArgumentException(MESSAGE_CARTS);
        }
        this.carts = carts;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }

    @Override
    public String print() {
        return String.format("Train ----" + System.lineSeparator() +
                "Passenger capacity: %d" + System.lineSeparator() +
                "Price per kilometer: %.2f" + System.lineSeparator() +
                "Vehicle type: %s" + System.lineSeparator() +
                "Carts amount: %d" + System.lineSeparator()
                 , this.passengerCapacity, this.pricePerKilometer, getType(), this.carts);
    }

    @Override
    public String toString() {
        return print();
    }
}
