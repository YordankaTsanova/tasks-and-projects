package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl implements Airplane {

    private int passengerCapacity;
    private double pricePerKilometer;

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setHasFreeFood(hasFreeFood);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }
    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.AIR;
    }

    @Override
    public String print() {
        return String.format("Airplane ----" + System.lineSeparator() +
                "Passenger capacity: %d" + System.lineSeparator() +
                "Price per kilometer: %.2f" + System.lineSeparator() +
                "Vehicle type: %s" + System.lineSeparator() +
                "Has free food: %s" + System.lineSeparator()
                , this.passengerCapacity, this.pricePerKilometer, getType(), hasFreeFood());
    }
    @Override
    public String toString() {
        return print();
    }
}
